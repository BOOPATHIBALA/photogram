var scene = document.getElementById("scene");
var parallaxInstance = new Parallax(scene, {
  scalarX: 2,
  scalarY: 2,
});
parallaxInstance.friction(1, 1);

function togglePassword() {
    var passwordField = document.getElementById("floatingPassword");
    var toggleIcon = document.getElementById("togglePasswordIcon");

    if (passwordField.type === "password") {
        passwordField.type = "text";
        toggleIcon.classList.remove("bi-eye-slash");
        toggleIcon.classList.add("bi-eye");
    } else {
        passwordField.type = "password";
        toggleIcon.classList.remove("bi-eye");
        toggleIcon.classList.add("bi-eye-slash");
    }
}